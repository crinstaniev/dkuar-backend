import os

import redis
from rq import Queue
from rq_scheduler import Scheduler


class Cache(object):
    def __init__(self):
        if os.environ.get('MODE') == 'dev':
            self.redis = redis.Redis(host='localhost')
        else:
            self.redis = redis.Redis(host='redis')
        self.queue = Queue(connection=self.redis)
        self.scheduler = Scheduler('sched', connection=self.redis)

    def init_app(self, app):
        app.logger.info('cache is ready')
        return
