import matplotlib.patches as patches
import matplotlib.pyplot as plt
from PIL import Image
import io


def expand2square(pil_img, background_color=(0, 0, 0)):
    width, height = pil_img.size
    if width == height:
        return pil_img
    elif width > height:
        result = Image.new(pil_img.mode, (width, width), background_color)
        result.paste(pil_img, (0, (width - height) // 2))
        return result
    else:
        result = Image.new(pil_img.mode, (height, height), background_color)
        result.paste(pil_img, ((height - width) // 2, 0))
        return result.resize((128, 128))


def image_grid(imgs, rows=5, cols=5):
    assert len(imgs) == (rows * cols)

    w, h = imgs[0].size
    grid = Image.new('RGB', size=(cols*w, rows*h))

    for i, img in enumerate(imgs):
        grid.paste(img, box=(i % cols*w, i//cols*h))
    return grid


def annotate(img, img_detection_info):
    plt.switch_backend('Agg')

    object_cnt = len(img_detection_info['boxes'])

    fig, ax = plt.subplots()
    ax.imshow(img)

    print(img_detection_info)

    for i in range(object_cnt):
        box = img_detection_info['boxes'][i]

        anchor = (box['x1'], box['y1'])
        x_dist = box['x2'] - box['x1']
        y_dist = box['y2'] - box['y1']

        rectangle = patches.Rectangle(
            anchor, x_dist, y_dist, edgecolor='r', linewidth=2, facecolor='none')

        ax.text(anchor[0], anchor[1] - 2, img_detection_info['labels'][i],
                size=10, bbox=dict(boxstyle='square', ec=(1., 0.5, 0.5),
                                   fc=(1., 0.8, 0.8)))

        ax.add_patch(rectangle)

    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    im = Image.open(buf)

    return im
