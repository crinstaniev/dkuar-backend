from core.utils.cache import Cache
from core.utils.db import Database
from core.utils.detector_model import DetectorModel

model = DetectorModel()
cache = Cache()
database = Database()
