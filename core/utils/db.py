import io
import os
import pickle
from datetime import datetime

import gridfs
import pymongo
from core.utils.get_timestamp import get_timestamp
from core.utils.images import annotate, expand2square, image_grid
from pymongo import MongoClient


class Database:
    def __init__(self):
        if os.environ.get('MODE') == 'dev':
            self.mongo = MongoClient('localhost', 27017, username='root',
                                     password='password')
        else:
            self.mongo = MongoClient('mongo', 27017, username='root',
                                     password='password')
        self.db = self.mongo.AR
        self.fs = gridfs.GridFS(self.db)

    def init_app(self, app):
        app.logger.info('database is ready')
        return


def upload_img(image, detection_result):
    timestamp = get_timestamp()
    image_stream = io.BytesIO()
    pickle.dump(image, image_stream)
    image_bytes = image_stream.getvalue()
    database = Database()

    file = database.fs.put(image_bytes, filename=timestamp)
    result = database.db.img.insert_one({
        'upload_time': datetime.now(),
        'image_object_id': file,
        'detection_result': detection_result
    })
    return result


def clean_db():
    database = Database()
    img_collection = database.db.get_collection('img')
    result = img_collection.find()
    result.sort('upload_time', pymongo.DESCENDING)
    result = result[25:]
    for item in result:
        database.fs.delete(item['image_object_id'])
        img_collection.delete_one({'_id': item['_id']})
    return


def fetch_grid():
    database = Database()
    img_collection = database.db.get_collection('img')
    result = img_collection.find()
    grid_ingred = result.sort('upload_time', pymongo.DESCENDING)[:25]
    images = []
    for item in grid_ingred:
        img_obj_id = item['image_object_id']
        img_detection_info = item['detection_result']
        img_byte = database.fs.find_one({'_id': img_obj_id})
        img_stream = io.BytesIO(img_byte.read())
        img = pickle.load(img_stream)

        img = annotate(img, img_detection_info)

        img = expand2square(img)
        images.append(img)
    grid = image_grid(images)

    return grid
