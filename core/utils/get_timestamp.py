from datetime import datetime


def get_timestamp():
    return datetime.utcnow().strftime('%Y%m%d%H%M%S%f')
