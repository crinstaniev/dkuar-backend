from pytorchyolo import detect, models


class DetectorModel():
    def __init__(self):
        # model init
        self.model = models.load_model(
            "data/yolov3.cfg",
            "data/yolov3.weights"
        )
        self.converter = CodeToNameConvertor()

    def init_app(self, app):
        app.logger.info('model is ready')
        return

    def detect(self, image):
        boxes = detect.detect_image(self.model, image)

        result = dict(boxes=[], labels=[], confidences=[])

        for item in boxes:
            x1, y1, x2, y2, confidence, label_code = item
            label_code = int(label_code)
            label_text = self.converter.code2text(label_code)

            box = dict(x1=int(x1), y1=int(y1), x2=int(x2), y2=int(y2))

            result["boxes"].append(box)
            result["labels"].append(label_text)
            result["confidences"].append(str(confidence))

        return result


class CodeToNameConvertor():
    def __init__(self):
        self.labels = self._load_labels()

    def _load_labels(self):
        f = open('data/coco.names', 'r')

        labels_raw = f.readlines()
        labels = []

        for label in labels_raw:
            label = label.strip()
            labels.append(label)

        return labels

    def code2text(self, code):
        return self.labels[code]
