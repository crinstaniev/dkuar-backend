import logging
from subprocess import Popen

from flask import Flask
from flask_cors import CORS
from core.utils.db import clean_db

import core.utils.extensions as extensions
from core.api import api
from core.utils import extensions


def create_app():
    app = Flask(__name__)

    CORS(app)

    app.register_blueprint(api)

    @app.after_request
    def after_request(response):
        response.headers.add("Access-Control-Allow-Origin", "*")
        response.headers.add(
            "Access-Control-Allow-Headers", "Content-Type, Authorization,x-api-key"
        )
        response.headers.add(
            "Access-Control-Allow-Methods", "GET,OPTION,PUT,POST,DELETE"
        )
        return response

    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    extensions.model.init_app(app)
    extensions.cache.init_app(app)
    extensions.database.init_app(app)

    return app
