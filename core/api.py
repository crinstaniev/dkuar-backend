import base64
import io
from datetime import datetime

import numpy as np
from flask import Blueprint, current_app, jsonify, render_template, request
from PIL import Image

from core.utils.db import clean_db, fetch_grid, upload_img
from core.utils.extensions import cache, model

api = Blueprint('api', __name__, url_prefix='/api')


@api.route('fetch', methods=['POST'])
def fetch():
    image = request.files['image']

    ext = image.filename.split('.')[-1]

    im = Image.open(image)

    width, height = im.size
    meta = dict(
        height=height,
        width=width,
        mode=im.mode,
        ext=ext,
    )
    np_img = np.array(im)

    time_start = datetime.now()
    detection_result = model.detect(np_img)
    time_end = datetime.now()

    time_elapsed = time_end - time_start
    with current_app.app_context():
        current_app.logger.debug(
            f'detection time: {(time_elapsed.total_seconds() * 1000):.0f} ms'
        )

    cache.queue.enqueue(upload_img, im, detection_result)

    return jsonify(
        dict(
            meta=meta,
            labels=detection_result['labels'],
            confidences=detection_result['confidences'],
            boxes=detection_result['boxes']
        )
    )


@api.route('clean', methods=['GET'])
def clean():
    cache.queue.enqueue(clean_db)
    with current_app.app_context():
        current_app.logger.info('old images cleaned')
    return 'success'


@api.route('grid', methods=['GET'])
def grid():
    image = fetch_grid()
    image_buffer = io.BytesIO()
    image.save(image_buffer, format='JPEG')
    img_b64 = base64.b64encode(image_buffer.getvalue())
    b64_string = 'data:image/jpeg;base64, ' + img_b64.decode()
    return render_template('image_grid.html', img_url=str(b64_string))
