import requests

# BASE_URL = 'http://localhost:8080'
BASE_URL = 'http://localhost:8080'
IMG_FOLDER = 'test/img/'


def test_upload_file():
    print('\n')

    for _ in range(4):
        for i in range(8):
            img_path = f'{IMG_FOLDER}{str(i + 1).zfill(3)}.jpg'
            image = open(img_path, 'rb')
            result = requests.post(BASE_URL + '/api/fetch',
                                   files=dict(image=image))
            # print(result.content.decode())
            print('SUCCESS')

    return
