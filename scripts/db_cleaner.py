import requests
import schedule

BASE_URL = 'http://localhost:8080/api'


def clean():
    try:
        requests.get(BASE_URL + '/clean')
    except Exception:
        print('clean db failed')


schedule.every(60).seconds.do(clean)

while True:
    schedule.run_pending()
