FROM python:3.8.5-slim-buster

RUN apt-get update
RUN apt-get install -y build-essential gcc python3-dev libgl1 libglib2.0-0 libsm6 libxrender1 libxext6

RUN mkdir /app
WORKDIR /app

COPY ./requirements.txt /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . /app

ENV FLASK_APP=core

EXPOSE 5000
EXPOSE 8080

RUN chmod +x ./scripts/run_worker.sh

CMD ["make", "prod"]
