dev:
	export FLASK_APP=core
	export FLASK_ENV=development
	MODE=dev flask run

requirements:
	pip freeze > requirements.txt

prod:
	@# uwsgi --http 127.0.0.1:8000 --master -p 4 -w wsgi:app
	@# waitress-serve --call core:create_app
	python ./scripts/db_cleaner.py&
	./scripts/run_worker.sh&
	gunicorn --workers=1 'core:create_app()' --bind 0.0.0.0:8080 --log-level debug

test:
	@pytest -v -s

.PHONY: dev requirements prod test